# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Product.delete_all
#...
Product.create!(title: 'Programming ruby 1.9 & 2.0',description: %{<p> Ruby is the fastest growing and most exciting dynamic language out there. 
	If you need to get working programs delivered fast,you should add Ruby to your toolbox.</p>},image_url: 'prag.png',price: 49.95) 
Product.create!(title: 'Ruby on rails',description: %{<p> Rails just keeps on changing. Both Rails 3 and 4, as well as Ruby 1.9 and 2.0, bring hundreds of improvements, 
	including new APIs and substantial performance enhancements. The fourth edition of this award-winning classic has been reorganized and 
	refocused so it’s more useful than ever before for developers new to Ruby and Rails.</p>},image_url: 'rails.png',price: 22.45) 
Product.create!(title: 'HTML5 and CSS3',description: %{<p> HTML5 and CSS3 are the future of web development, but you don’t have to wait to start using them. Even though the specification is still in development,
 many modern browsers and mobile devices already support HTML5 and CSS3.</p>},image_url: 'html.png',price: 54.45) 
# . . .