class LineItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :product
  belongs_to :cart

  def total_price
    product.price * quantity  # calculate the total price for Each line item 
  	# price * quantity # use the line item price to calculate the total price instead of the product price. in case we change adjust the product price
  end
end

