class Order < ActiveRecord::Base
	has_many :line_items, dependent: :destroy
   PAYMENT_TYPES = [ "Check", "Credit card", "Purchase order" ]
   validates :name, :address, :email, presence: true
   validates :pay_type, inclusion: PAYMENT_TYPES
   after_update :send_new_ship_email, :if => :ship_date_changed? && :no_ship_date # send first email notification 
   after_update :send_changed_ship_email, :if => :ship_date_changed? && :ship_date_was # send changed email notification
  def add_line_items_from_cart(cart)
    # debugger
  	cart.line_items.each do |item|
  		item.cart_id = nil
  		line_items << item
    end
  end
  
  def no_ship_date
    self.ship_date_was.nil?
  end

  def send_new_ship_email
    OrderNotifier.shipped(self).deliver_now
  end

  def send_changed_ship_email
    OrderNotifier.changed_shipping(self).deliver_now
  end
end
