module CurrentCart
	extend ActiveSupport::Concern

	private

	def set_cart
		# debugger
		@cart = Cart.find(session[:cart_id]) # retrieve the cart from the seesion
	rescue ActiveRecord::RecordNotFound    # handle redcordNotfound error 
		@cart = Cart.create          # create new cart
		session[:cart_id] = @cart.id  # place new cart_id in the session
	end
end