module ApplicationHelper
	def hidden_div_if(condition, attributes = {}, &block)  #disable css styling if the cart is empty
		if condition
			attributes["style"] = "display: none"
		end
		content_tag("div", attributes, &block)
	end
end
