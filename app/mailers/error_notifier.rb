class ErrorNotifier < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.error_notifier.error_occured.subject
  #
  def error_occured
    #@error = error

    mail :to => "admin@depot.com", :subject => 'Depot App Error Incident'
  end
end
