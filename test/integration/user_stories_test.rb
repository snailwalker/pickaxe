	require 'test_helper'

	class UserStoriesTest < ActionDispatch::IntegrationTest
		fixtures :products
	   test "should fail on access of sensitive data" do
	   	user = users(:one)
	   	get "/login"
	   	assert_response :success
	   	post_via_redirect "/login", name: user.name, password: 'secret'
	   	assert_response :success
	   	assert_equal '/admin', path
	   	get "/carts/12345"
	   	assert_response :success
	   	assert_equal '/carts/12345', path

	   	delete "/logout"
	   	assert_response :redirect
	   	#assert_equal '', path

	   	get "/carts/12345"
	   	assert_response :redirect
	   	follow_redirect!
	   	assert_equal '/login', path
	   end
	   test "Buying a product" do
		LineItem.delete_all
		Order.delete_all
		ruby_book = products(:ruby)

		get "/"
		assert_response :success
		assert_template "index"

		xml_http_request :post, '/line_items', product_id: ruby_book.id
		assert_response :success

		cart = Cart.find(session[:cart_id])
		assert_equal 1, cart.line_items.size
		assert_equal ruby_book, cart.line_items[0].product

		get "/orders/new"
		assert_response :success
		assert_template "new"
		ship_date_expected = Time.now.to_date # new line
		post_via_redirect "/orders",
		                 order: { name: "Dave Thomas",
		                          address: "123 The Street",
		                          email: "dave@example.com",
		                          pay_type: "Check",
		                          ship_date: Time.now.to_date }
		assert_response :success
		assert_template "index"
		cart = Cart.find(session[:cart_id])
		assert_equal 0, cart.line_items.size

		orders = Order.all
		assert_equal 1, orders.size
		order = orders[0]

		assert_equal "Dave Thomas", order.name
		assert_equal "123 The Street", order.address
		assert_equal "dave@example.com", order.email
		assert_equal "Check", order.pay_type
		assert_equal ship_date_expected, order.ship_date #new line

		assert_equal 1, order.line_items.size
		line_item = order.line_items[0]
		#debugger
		assert_equal ruby_book, line_item.product

		mail = ActionMailer::Base.deliveries.last
		assert_equal ["dave@example.com"], mail.to
		assert_equal 'Sam Ruby <depot@example.com>', mail[:from].value
		assert_equal "Pragmatic Store Order Confirmation", mail.subject
	end


	   test "should mail the admin when error occurs" do
	   	get "/carts/wibble"
	   	assert_response :redirect
	   	#assert_template "login"

	    mail = ErrorNotifier.error_occured
	    assert_equal "Depot App Error Incident", mail.subject
	    assert_equal ["admin@depot.com"], mail.to
	    assert_equal ["from@example.com"], mail.from
	    assert_match "Hello Admin", mail.body.encoded  #match with some of the content
	end
end


	

