require 'test_helper'

class ErrorNotifierTest < ActionMailer::TestCase
  test "should mail the admin when error occurs" do

    mail = ErrorNotifier.error_occured
    assert_equal "Depot App Error Incident", mail.subject
    assert_equal ["admin@depot.com"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hello Admin", mail.body.encoded  #match with some of the content .
  end
end
 
  	



